<?php
/**
 * @file
 * Module file for Size Restriction Image Widget with Media.
 */

use Drupal\file\FileInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetInterface;

/**
 * Implements hook_field_widget_third_party_settings_form().
 */
function size_restriction_image_widget_field_widget_third_party_settings_form(WidgetInterface $plugin, FieldDefinitionInterface $field_definition, $form_mode, $form, FormStateInterface $form_state) {
  $element = [];
  $plugin_id = $plugin->getPluginId();
  if ($plugin_id == 'media_library_widget') {
    $settings = $plugin->getThirdPartySettings('size_restriction_image_widget');
    $element['image_settings'] = [
      '#type' => 'details',
      '#title' => t('Image Size Requirements'),
      '#open' => TRUE,
    ];

    $max_resolution = [
      'x' => '',
      'y' => '',
    ];
    $min_resolution = [
      'x' => '',
      'y' => '',
    ];

    if (array_key_exists('image_settings', $settings)) {
      // Add maximum and minimum resolution settings.
      if (!empty($settings['image_settings']['max_resolution'])) {
        $max_resolution['x'] = $settings['image_settings']['max_resolution']['x'];
        $max_resolution['y'] = $settings['image_settings']['max_resolution']['y'];
      }
      if (!empty($settings['image_settings']['min_resolution'])) {
        $min_resolution['x'] = $settings['image_settings']['min_resolution']['x'];
        $min_resolution['y'] = $settings['image_settings']['min_resolution']['y'];
      }
    }

    $element['image_settings']['max_resolution'] = [
      '#type' => 'item',
      '#title' => t('Maximum image resolution'),
      '#weight' => 4.1,
      '#description' => t('The maximum allowed image size expressed as WIDTH×HEIGHT (e.g. 640×480). Leave blank for no restriction. If a larger image is uploaded, it will be resized to reflect the given width and height. Resizing images on upload will cause the loss of <a href="http://wikipedia.org/wiki/Exchangeable_image_file_format">EXIF data</a> in the image.'),
    ];
    $element['image_settings']['max_resolution']['x'] = [
      '#type' => 'number',
      '#title' => t('Maximum width'),
      '#title_display' => 'invisible',
      '#default_value' => $max_resolution['x'],
      '#min' => 1,
      '#field_suffix' => ' × ',
      '#prefix' => '<div class="form--inline clearfix">',
    ];
    $element['image_settings']['max_resolution']['y'] = [
      '#type' => 'number',
      '#title' => t('Maximum height'),
      '#title_display' => 'invisible',
      '#default_value' => $max_resolution['y'],
      '#min' => 1,
      '#field_suffix' => ' ' . t('pixels'),
      '#suffix' => '</div>',
    ];

    $element['image_settings']['min_resolution'] = [
      '#type' => 'item',
      '#title' => t('Minimum image resolution'),
      '#weight' => 4.2,
      '#description' => t('The minimum allowed image size expressed as WIDTH×HEIGHT (e.g. 640×480). Leave blank for no restriction. If a smaller image is uploaded, it will be rejected.'),
    ];
    $element['image_settings']['min_resolution']['x'] = [
      '#type' => 'number',
      '#title' => t('Minimum width'),
      '#title_display' => 'invisible',
      '#default_value' => $min_resolution['x'],
      '#min' => 1,
      '#field_suffix' => ' × ',
      '#prefix' => '<div class="form--inline clearfix">',
    ];
    $element['image_settings']['min_resolution']['y'] = [
      '#type' => 'number',
      '#title' => t('Minimum height'),
      '#title_display' => 'invisible',
      '#default_value' => $min_resolution['y'],
      '#min' => 1,
      '#field_suffix' => ' ' . t('pixels'),
      '#suffix' => '</div>',
    ];
  }

  return $element;
}

/**
 * Implements hook_field_widget_settings_summary_alter().
 */
function size_restriction_image_widget_field_widget_settings_summary_alter(&$summary, $context) {
  $plugin_id = $context['widget']->getPluginId();
  if ($plugin_id == 'media_library_widget') {
    $settings = $context['widget']->getThirdPartySettings('size_restriction_image_widget');
    if (isset($settings['image_settings']) && $settings['image_settings']) {
      $image_settings = _getImageSettings($settings['image_settings']);
      if ($image_settings['max'] || $image_settings['min']) {
        if (isset($image_settings['max']) && $image_settings['max']) {
          $summary[] = t('Maximum image resolution: @max', [
            '@max' => $image_settings['max'],
          ]);
        }
        if (isset($image_settings['min']) && $image_settings['min']) {
          $summary[] = t('Minimum image resolution: @min', [
            '@min' => $image_settings['min'],
          ]);
        }
      }
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function size_restriction_image_widget_field_widget_complete_form_alter(&$element, FormStateInterface $form_state, $context) {
  $widget = $context['widget'];
  if (($widget->getPluginId() === 'media_library_widget')) {
    $settings = $widget->getThirdPartySettings('size_restriction_image_widget');
    if (!empty($widget) && !empty($settings)) {
      $field_name = $context['items']->getName();
      $triggering_element = $form_state->getTriggeringElement();
      if ($triggering_element) {
        if (str_contains($triggering_element['#name'], $field_name)) {
          // Get tempstore service.
          $tempstore = Drupal::service('tempstore.private');
          // Get tempstore data we need.
          $tempstore_data = $tempstore->get('size_restriction_image_widget');
          $params = $tempstore_data->get('params');
          // Fill vars.
          $params['size_restriction_image_widget']['max_resolution'] = $settings['image_settings']['max_resolution'];
          $params['size_restriction_image_widget']['min_resolution'] = $settings['image_settings']['min_resolution'];

          // Save vars to tempstore.
          $tempstore_data->set('params', $params);
        }
      }
    }
  }
}

function size_restriction_image_widget_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'media_library_add_form_upload') {
    $tempstore_params = _getTempStoreParams();
    if ($tempstore_params) {
      // Set the validation
      $form['container']['upload']['#upload_validators']['_validate_image_dimensions'] = [$tempstore_params];
      if (!empty($form['container']['upload']['#description'])) {
        // Add process callback to change field description.
        $form['container']['upload']['#process'][] = '_change_description';
      }
    }
  }
}

/**
 * Validate article image.
 *
 * @param \Drupal\file\FileInterface $file
 *   File object.
 *
 * @return array
 *   Errors array.
 */
function _validate_image_dimensions(FileInterface $file, $tempstore_params) {
  $errors = [];
  $image = Drupal::service('image.factory')->get($file->getFileUri());
  if ($image->isValid()) {
    // Validate based on the min / max dimensions set by the field widget
    $image_settings = _getImageSettings($tempstore_params['size_restriction_image_widget']);
    $errors = file_validate_image_resolution($file, $image_settings['max'], $image_settings['min']);
  }
  return $errors;
}

/**
 *  The description field is built in a #process callback, which is why we need
 * to add another callback.
 */
function _change_description($element, FormStateInterface $form_state, $form) {
  $tempstore_params = _getTempStoreParams();

  if ($tempstore_params) {
    $image_settings = _getImageSettings($tempstore_params['size_restriction_image_widget']);

    $file_resolution_description = '';
    $file_size_description = '';
    $file_extension_description = '';

    if (isset($form['container']['upload']['#upload_validators']['file_validate_extensions'])) {
      $file_extension_description = 'Allowed types: <strong>' . $form['container']['upload']['#upload_validators']['file_validate_extensions'][0] . '</strong><br />';
    }
    if (isset($form['container']['upload']['#upload_validators']['file_validate_size'])) {
      $file_size_description = 'Files must be less than  <strong>' . format_size($form['container']['upload']['#upload_validators']['file_validate_size'][0]) . '</strong><br />';
    }

    if ($image_settings['min'] && $image_settings['max'] && $image_settings['min'] == $image_settings['max']) {
      $file_resolution_description = 'Images must be exactly <strong>' . $image_settings['max'] . '</strong> pixels.';
    }
    elseif ($image_settings['min'] && $image_settings['max']) {
      $file_resolution_description = 'Images must be larger than <strong>' . $image_settings['min'] . '</strong> pixels. Images larger than <strong>' . $image_settings['max'] . '</strong> pixels will be resized.';
    }
    elseif ($image_settings['min']) {
      $file_resolution_description = 'Images must be larger than <strong>' . $image_settings['min'] . '</strong> pixels.';
    }
    elseif ($image_settings['max']) {
      $file_resolution_description = 'Images larger than <strong>' . $image_settings['max'] . '</strong> pixels will be resized.';
    }
    $element['#description'] = FieldFilteredMarkup::create(
      $file_size_description
      . $file_extension_description
      . $file_resolution_description
    );
  }
  return $element;
}

/**
 *  helper function to get the image dimensions from the tempstore.
 */
function _getTempStoreParams() {
  $tempstore = Drupal::service('tempstore.private');
  $tempstore_data = $tempstore->get('size_restriction_image_widget');
  $tempstore_params = $tempstore_data->get('params');
  return $tempstore_params;
}

/**
 *  helper function that returns image dimensions reformatted for easier logic
 * checks in the module.
 *
 * @return array
 *   Settings array.
 *
 */
function _getImageSettings($settings): array {
  $image_settings = [];
  $max = $settings['max_resolution'];
  $min = $settings['min_resolution'];

  if (empty($max['x']) && empty($max['y'])) {
    $max = 0;
  }
  else {
    $max = $max['x'] . 'x' . $max['y'];
  }
  if (empty($min['x']) && empty($min['y'])) {
    $min = 0;
  }
  else {
    $min = $min['x'] . 'x' . $min['y'];
  }
  $image_settings['max'] = $max;
  $image_settings['min'] = $min;

  return $image_settings;
}
