CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation

INTRODUCTION
------------

This module modifies image form widgets to allow site builders to provide image dimensions restrictions.


INSTALLATION
------------

The installation of this module is like other Drupal modules.

1. Copy/upload the module to the modules directory of your Drupal
   installation.

2. Enable the 'Size Restriction Image Widget' module in 'Extend'.
   (/admin/_modules)_

4. Edit the form display settings for the media and put in your
   desired maximum and minimum image restrictions.

5. Add an image and observe you users cannot upload images that do not
   meet the image size requirements.
